http://chriswu.me/blog/writing-hello-world-in-fcgi-with-c-plus-plus/

sudo apt-get install libfcgi-dev
sudo apt-get install spawn-fcgi
sudo apt-get install nginx
sudo apt-get install curl

sudo cp nginx.conf /usr/share/nginx/

sudo killall nginx
sudo nginx -c /usr/share/nginx/nginx.conf

Compile the file with below command
g++ hello_server.cpp  -lfcgi++ -lfcgi -o hello_server
then run it
spawn-fcgi -p 8000 -n hello_server

open the browser: enter http://localhost/ into the location bar  